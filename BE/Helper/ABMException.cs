﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.Helper
{
    public class ABMException : Exception
    {
        public ABMException(string mensaje) : base(mensaje)
        {

        }
    }
}
