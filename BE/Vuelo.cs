﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Vuelo
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private Origen _origen;
        public Origen Origen
        {
            get { return _origen; }
            set { _origen = value; }
        }

        private Destino _destino;

        public Destino Destino
        {
            get { return _destino; }
            set { _destino = value; }
        }

        private List<Escala> _escalas;

        public List<Escala> Escalas
        {
            get { return _escalas; }
            set { _escalas = value; }
        }

        private List<Pasajero> _pasajeros;

        public List<Pasajero> Pasajeros
        {
            get { return _pasajeros; }
            set { _pasajeros = value; }
        }

        private int _cantidadPasajerosLimite;

        public int CantidadPasajerosLimite
        {
            get { return _cantidadPasajerosLimite; }
            set { _cantidadPasajerosLimite = value; }
        }

    }
}