﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Origen : Ciudad
    {
        private int _id;

        public int IdOrigen
        {
            get { return _id; }
            set { _id = value; }
        }
        public Origen(Ciudad _ciudad)
        {
            CrearOrigen(_ciudad);
        }

        private DateTime _despegue;

        public DateTime Despegue
        {
            get { return _despegue; }
            set { _despegue = value; }
        }

        private void CrearOrigen(Ciudad ciudad)
        {
            IdCiudad = ciudad.IdCiudad;
            Latitud = ciudad.Latitud;
            Localidad = ciudad.Localidad;
            Longitud = ciudad.Longitud;
            Pais = ciudad.Pais;
        }
    }
}