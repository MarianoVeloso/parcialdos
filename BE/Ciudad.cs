﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Ciudad
    {
        private int _id;

        public int IdCiudad
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _localidad;

        public string Localidad
        {
            get { return _localidad; }
            set { _localidad = value; }
        }

        private string _pais;

        public string Pais
        {
            get { return _pais; }
            set { _pais = value; }
        }

        private string _latitud;

        public string Latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }

        private string _longitud;

        public string Longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }

        public override string ToString()
        {
            return _pais + " - " + _localidad;
        }
    }
}