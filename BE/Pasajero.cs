﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Pasajero
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _nombre;

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        private string _apellido;

        public string Apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }

        private long _dni;

        public long DNI
        {
            get { return _dni; }
            set { _dni = value; }
        }

        public override string ToString()
        {
            return Nombre + " " + Apellido;
        }
    }
}