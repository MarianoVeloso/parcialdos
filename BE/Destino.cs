﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Destino : Ciudad
    {
        public Destino(Ciudad _ciudad)
        {
            CrearOrigen(_ciudad);
        }


        private int _id;

        public int IdDestino
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _Arribo;

        public DateTime Arribo
        {
            get { return _Arribo; }
            set { _Arribo = value; }
        }

        private bool _internacional;

        public bool Internacional
        {
            get { return _internacional; }
            set { _internacional = value; }
        }

        private void CrearOrigen(Ciudad ciudad)
        {
            IdCiudad = ciudad.IdCiudad;
            Latitud = ciudad.Latitud;
            Localidad = ciudad.Localidad;
            Longitud = ciudad.Longitud;
            Pais = ciudad.Pais;
        }
    }
}