﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Escala : Ciudad
    {
        public Escala(Ciudad _ciudad)
        {
            CrearEscala(_ciudad);
        }
        private DateTime _arribo;

        public DateTime Arribo
        {
            get { return _arribo; }
            set { _arribo = value; }
        }

        private DateTime _despegue;

        public DateTime Despegue
        {
            get { return _despegue; }
            set { _despegue = value; }
        }

        private void CrearEscala(Ciudad ciudad)
        {
            IdCiudad = ciudad.IdCiudad;
            Latitud = ciudad.Latitud;
            Localidad = ciudad.Localidad;
            Longitud = ciudad.Longitud;
            Pais = ciudad.Pais;
        }

    }
}