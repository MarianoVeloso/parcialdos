﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CiudadBusiness
    {
        private readonly CiudadMapper _ciudadMapper = new CiudadMapper();

        public List<Ciudad> Listar()
        {
            return _ciudadMapper.Listar();
        }

        public void Crear(Ciudad ciudad)
        {
            if (ciudad.IdCiudad == 0)
            {
                _ciudadMapper.Insertar(ciudad);
            }
            else
            {
                Editar(ciudad);
            }
        }

        public void Editar(Ciudad ciudad)
        {
            _ciudadMapper.Editar(ciudad);
        }

        public void Borrar(Ciudad ciudad)
        {
            _ciudadMapper.Borrar(ciudad);
        }
    }
}
