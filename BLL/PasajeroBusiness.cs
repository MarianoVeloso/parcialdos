﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PasajeroBusiness
    {
        private readonly PasajeroMapper _pasajeroMapper = new PasajeroMapper();

        public List<Pasajero> Listar()
        {
            return _pasajeroMapper.Listar();
        }

        public void Crear(Pasajero pasajero)
        {
            if(pasajero.Id == 0)
            {
                _pasajeroMapper.Insertar(pasajero);
            }
            else
            {
                Editar(pasajero);
            }
        }

        public void Editar(Pasajero pasajero)
        {
            _pasajeroMapper.Editar(pasajero);
        }

        public void Borrar(Pasajero pasajero)
        {
            _pasajeroMapper.Borrar(pasajero);
        }
    }
}
