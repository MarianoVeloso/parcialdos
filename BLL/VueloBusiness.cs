﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class VueloBusiness
    {
        private readonly VueloMapper _vueloMapper = new VueloMapper();

        public void Crear(Vuelo vuelo)
        {
            _vueloMapper.InsertarOrigen(vuelo.Origen);
            _vueloMapper.InsertarDestino(vuelo.Destino);
            _vueloMapper.Insertar(vuelo);

            
            foreach (var item in vuelo.Pasajeros)
            {
                _vueloMapper.InsertarPasajeroEnVuelo(vuelo.Id, item.Id);
            }
        }
    }
}
