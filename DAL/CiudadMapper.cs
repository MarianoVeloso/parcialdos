﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CiudadMapper : GenericMapper
    {

        public List<Ciudad> Listar()
        {
            DataTable tablaCiudades = Leer("ciudad_obtener");

            List<Ciudad> ciudades = new List<Ciudad>();

            foreach (DataRow registroConcepto in tablaCiudades.Rows)
            {
                Ciudad ciudad = MapearCiudad(registroConcepto);

                ciudades.Add(ciudad);
            }
            return ciudades;
        }

        public int Insertar(Ciudad ciudad)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@localidad", ciudad.Localidad),
                CrearParametro("@pais", ciudad.Pais),
                CrearParametro("@longitud", ciudad.Longitud),
                CrearParametro("@latitud", ciudad.Latitud),
            };
            return RealizarOperacion("ciudad_insertar", parameters);
        }

        public void Editar(Ciudad ciudad)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@id", ciudad.IdCiudad),
                CrearParametro("@localidad", ciudad.Localidad),
                CrearParametro("@pais", ciudad.Pais),
                CrearParametro("@longitud", ciudad.Longitud),
                CrearParametro("@latitud", ciudad.Latitud),
            };
            RealizarOperacion("ciudad_editar", parameters);
        }

        public void Borrar(Ciudad ciudad)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@id", ciudad.IdCiudad)
            };
            RealizarOperacion("ciudad_borrar", parameters);
        }

        public Ciudad MapearCiudad(DataRow row)
        {
            return new Ciudad()
            {
                IdCiudad = int.Parse(row["id"].ToString()),
                Pais = row["pais"].ToString(),
                Localidad = row["localidad"].ToString(),
                Latitud = row["latitud"].ToString(),
                Longitud = row["longitud"].ToString()
            };
        }
    }
}
