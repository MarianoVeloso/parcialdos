﻿using BE;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class VueloMapper : GenericMapper
    {
        public void Insertar(Vuelo vuelo)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@idOrigen", vuelo.Origen.IdOrigen),
                CrearParametro("@idDestino", vuelo.Destino.IdDestino),
                CrearParametro("@id", vuelo.Id, ParameterDirection.Output)
            };

            RealizarOperacion("vuelo_insertar", parameters);

            vuelo.Id = int.Parse(parameters[2].Value.ToString());
        }

        public void InsertarDestino(Destino destino)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@idCiudad", destino.IdCiudad),
                CrearParametro("@arribo", destino.Arribo),
                CrearParametro("@internacional", destino.Internacional),
                CrearParametro("@id", destino.IdCiudad, ParameterDirection.Output)
            };

            RealizarOperacion("vuelo_insertar_destino", parameters);

            destino.IdDestino = int.Parse(parameters[3].Value.ToString());
        }

        public void InsertarOrigen(Origen origen)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@idCiudad", origen.IdCiudad),
                CrearParametro("@despegue", origen.Despegue),
                CrearParametro("@id", origen.IdCiudad, ParameterDirection.Output)
            };

            RealizarOperacion("vuelo_insertar_origen", parameters);

            origen.IdOrigen = int.Parse(parameters[2].Value.ToString());
        }

        public int InsertarPasajeroEnVuelo(int vuelo, int pasajero)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@idvuelo", vuelo),
                CrearParametro("@idPasajero", pasajero)
            };
            return RealizarOperacion("vuelo_insertar_pasajero", parameters);
        }
    }
}
