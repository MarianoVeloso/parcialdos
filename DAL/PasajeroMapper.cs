﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PasajeroMapper : GenericMapper
    {
        public List<Pasajero> Listar()
        {
            DataTable tablaCiudades = Leer("pasajero_obtener");

            List<Pasajero> pasajeros = new List<Pasajero>();

            foreach (DataRow registroConcepto in tablaCiudades.Rows)
            {
                Pasajero pasajero = MapearPasajero(registroConcepto);

                pasajeros.Add(pasajero);
            }
            return pasajeros;
        }

        public void Insertar(Pasajero pasajero)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@apellido", pasajero.Apellido),
                CrearParametro("@nombre", pasajero.Nombre),
                CrearParametro("@dni", pasajero.DNI)
            };
            RealizarOperacion("pasajero_insertar", parameters);
        }

        public void Borrar(Pasajero pasajero)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@Id", pasajero.Id)
            };
            RealizarOperacion("pasajero_borrar", parameters);
        }

        public void Editar(Pasajero pasajero)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                CrearParametro("@apellido", pasajero.Apellido),
                CrearParametro("@nombre", pasajero.Nombre),
                CrearParametro("@dni", pasajero.DNI)
            };
            RealizarOperacion("pasajero_editar", parameters);
        }

        public Pasajero MapearPasajero(DataRow row)
        {
            return new Pasajero()
            {
                Id = int.Parse(row["id"].ToString()),
                Apellido = row["apellido"].ToString(),
                Nombre = row["nombre"].ToString(),
                DNI = long.Parse(row["dni"].ToString())
            };
        }
    }
}
