﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
    internal class Acceso
    {
        private SqlConnection conexion;
        private SqlTransaction transaccion;
        public void Abrir()
        {
            conexion = new SqlConnection("Initial Catalog=parcialDos; Data Source=.\\SqlExpress; Integrated Security=SSPI");
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        public SqlParameter CrearParametro(string nombre, object valor, ParameterDirection direccion = ParameterDirection.Input)
        {
            DbType type;

            if (valor.GetType().Equals(typeof(bool)))
            {
                type = DbType.Byte;
            }
            else if (valor.GetType().Equals(typeof(string)))
            {
                type = DbType.String;
            }
            else if (valor.GetType().Equals(typeof(int)))
            {
                type = DbType.Int32;
            }
            else if (valor.GetType().Equals(typeof(DateTime)))
            {
                type = DbType.DateTime;
            }
            else if (valor.GetType().Equals(typeof(decimal)))
            {
                type = DbType.Decimal;
            }
            else
            {
                type = DbType.String;
            }

            SqlParameter p = new SqlParameter(nombre, valor)
            {
                DbType = type,
                Direction = direccion
            };
            return p;
        }
        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {
            SqlCommand comando = new SqlCommand(sql, conexion);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;
        }

        public DataTable Leer(string nombre, List<SqlParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            Abrir();
            adaptador.SelectCommand = CrearComando(nombre, parametros, CommandType.StoredProcedure);
            DataTable Tabla = new DataTable();
            adaptador.Fill(Tabla);

            Cerrar();
            return Tabla;
        }

        public int Operar(string nombre, List<SqlParameter> parametros)
        {
            SqlCommand comando = CrearComando(nombre, parametros, CommandType.StoredProcedure);
            int resultado;

            if (transaccion != null)
            {
                comando.Transaction = transaccion;
            }

            try
            {
                resultado = comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CancelarTransaccion();
                resultado = -1;
            }

            return resultado;
        }

        public void IniciarTransaccion()
        {
            if (transaccion != null && conexion.State == ConnectionState.Open)
            {
                transaccion = conexion.BeginTransaction();
            }
            else
            {
                transaccion = conexion.BeginTransaction();
            }
        }

        public void ConfirmarTransaccion()
        {
            transaccion.Commit();
            transaccion.Dispose();
            transaccion = null;

        }
        public void CancelarTransaccion()
        {
            transaccion.Rollback();
        }
    }
}
