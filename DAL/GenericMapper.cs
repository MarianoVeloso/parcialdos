﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class GenericMapper
    {
        private readonly Acceso acceso = new Acceso();
        internal int RealizarOperacion(string sql, List<SqlParameter> parameters = null)
        {
            acceso.Abrir();
            acceso.IniciarTransaccion();

            int res = acceso.Operar(sql, parameters);

            if (res == 1)
            {
                acceso.ConfirmarTransaccion();
            }
            else
            {

            }

            acceso.Cerrar();

            return res;
        }

        internal SqlParameter CrearParametro(string nombre, object valor, ParameterDirection direccion = ParameterDirection.Input)
        {
            return acceso.CrearParametro(nombre, valor, direccion);
        }

        internal DataTable Leer(string nombre, List<SqlParameter> parametros = null)
        {
            return acceso.Leer(nombre, parametros);
        }
    }
}
