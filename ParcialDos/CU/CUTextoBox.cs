﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ParcialDos.CU
{
    public partial class CUTextoBox : UserControl
    {
        private string _validacion;

        public string Validacion
        {
            get { return _validacion; }
            set { _validacion = value; }
        }


        private string _texto;

        public string Texto
        {
            get { return _texto; }
            set { _texto = value; }
        }

        private bool _validado;

        public bool Validado
        {
            get { return _validado; }
            set { _validado = value; }
        }

        public CUTextoBox()
        {
            InitializeComponent();
        }

        private void CUTextoBox_Load(object sender, EventArgs e)
        {
            lblText.Text = _texto;
        }

        public string ObtenerValor()
        {
            return txtBox.Text;
        }

        public void SetearValor(string valor)
        {
            txtBox.Text = valor;
        }

        private void txtBox_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_validacion))
            {
                var match = Regex.Match(txtBox.Text, _validacion);

                if (!match.Success)
                {
                    txtBox.BackColor = Color.Red;
                    _validado = false;
                }
                else
                {
                    _validado = true;
                    txtBox.BackColor = Color.White;
                }
            }
            else
            {
                _validado = true;
            }
        }
    }
}
