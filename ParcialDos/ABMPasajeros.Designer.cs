﻿namespace ParcialDos
{
    partial class ABMPasajeros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAlta = new System.Windows.Forms.GroupBox();
            this.btnCrear = new System.Windows.Forms.Button();
            this.cuNombre = new ParcialDos.CU.CUTextoBox();
            this.cuDNI = new ParcialDos.CU.CUTextoBox();
            this.cuApellido = new ParcialDos.CU.CUTextoBox();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.lstbxPasajeros = new System.Windows.Forms.ListBox();
            this.cuId = new ParcialDos.CU.CUTextoBox();
            this.gbxAlta.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxAlta
            // 
            this.gbxAlta.Controls.Add(this.cuId);
            this.gbxAlta.Controls.Add(this.btnCrear);
            this.gbxAlta.Controls.Add(this.cuNombre);
            this.gbxAlta.Controls.Add(this.cuDNI);
            this.gbxAlta.Controls.Add(this.cuApellido);
            this.gbxAlta.Location = new System.Drawing.Point(12, 12);
            this.gbxAlta.Name = "gbxAlta";
            this.gbxAlta.Size = new System.Drawing.Size(170, 195);
            this.gbxAlta.TabIndex = 5;
            this.gbxAlta.TabStop = false;
            this.gbxAlta.Text = "Alta";
            // 
            // btnCrear
            // 
            this.btnCrear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrear.Location = new System.Drawing.Point(89, 166);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(75, 23);
            this.btnCrear.TabIndex = 4;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = true;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // cuNombre
            // 
            this.cuNombre.Location = new System.Drawing.Point(15, 19);
            this.cuNombre.Name = "cuNombre";
            this.cuNombre.Size = new System.Drawing.Size(133, 39);
            this.cuNombre.TabIndex = 0;
            this.cuNombre.Texto = "Nombre";
            this.cuNombre.Validacion = "^[a-zA-Z]+$";
            this.cuNombre.Validado = false;
            // 
            // cuDNI
            // 
            this.cuDNI.Location = new System.Drawing.Point(14, 109);
            this.cuDNI.Name = "cuDNI";
            this.cuDNI.Size = new System.Drawing.Size(133, 39);
            this.cuDNI.TabIndex = 2;
            this.cuDNI.Texto = "DNI";
            this.cuDNI.Validacion = "\\b\\d{8}\\b";
            this.cuDNI.Validado = false;
            // 
            // cuApellido
            // 
            this.cuApellido.Location = new System.Drawing.Point(15, 64);
            this.cuApellido.Name = "cuApellido";
            this.cuApellido.Size = new System.Drawing.Size(133, 39);
            this.cuApellido.TabIndex = 1;
            this.cuApellido.Texto = "Apellido";
            this.cuApellido.Validacion = "^[a-zA-Z]+$";
            this.cuApellido.Validado = false;
            // 
            // btnEditar
            // 
            this.btnEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar.Location = new System.Drawing.Point(472, 219);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 10;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBorrar.Location = new System.Drawing.Point(553, 219);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(75, 23);
            this.btnBorrar.TabIndex = 9;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // lstbxPasajeros
            // 
            this.lstbxPasajeros.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lstbxPasajeros.FormattingEnabled = true;
            this.lstbxPasajeros.Location = new System.Drawing.Point(296, 20);
            this.lstbxPasajeros.Name = "lstbxPasajeros";
            this.lstbxPasajeros.Size = new System.Drawing.Size(332, 186);
            this.lstbxPasajeros.TabIndex = 8;
            // 
            // cuId
            // 
            this.cuId.Location = new System.Drawing.Point(108, 8);
            this.cuId.Name = "cuId";
            this.cuId.Size = new System.Drawing.Size(25, 18);
            this.cuId.TabIndex = 5;
            this.cuId.Texto = null;
            this.cuId.Validacion = null;
            this.cuId.Validado = true;
            this.cuId.Visible = false;
            // 
            // ABMPasajeros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 280);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.lstbxPasajeros);
            this.Controls.Add(this.gbxAlta);
            this.Name = "ABMPasajeros";
            this.Text = "ABMPasajeros";
            this.Load += new System.EventHandler(this.ABMPasajeros_Load);
            this.gbxAlta.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAlta;
        private System.Windows.Forms.Button btnCrear;
        private CU.CUTextoBox cuNombre;
        private CU.CUTextoBox cuDNI;
        private CU.CUTextoBox cuApellido;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.ListBox lstbxPasajeros;
        private CU.CUTextoBox cuId;
    }
}