﻿using BE;
using BE.Helper;
using BLL;
using ParcialDos.CU;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcialDos
{
    public partial class ABMVuelos : Form
    {
        private List<Escala> _escalas;
        private List<Pasajero> _pasajeros;
        private readonly CiudadBusiness _ciudadBusiness = new CiudadBusiness();
        private readonly PasajeroBusiness _pasajeroBusiness = new PasajeroBusiness();
        private readonly VueloBusiness _vueloBusiness = new VueloBusiness();
        public ABMVuelos()
        {
            InitializeComponent();
        }

        private void ABMVuelos_Load(object sender, EventArgs e)
        {
            listCiudadOrigen.DataSource = null;
            listCiudadOrigen.DataSource = _ciudadBusiness.Listar();

            listCiudadDestino.DataSource = null;
            listCiudadDestino.DataSource = _ciudadBusiness.Listar();

            listEscala.DataSource = null;
            listEscala.DataSource = _ciudadBusiness.Listar();
            listEscala.SelectedItem = null;

            listPasajero.DataSource = null;
            listPasajero.DataSource = _pasajeroBusiness.Listar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //ValidarControles(); 
                ValidarVuelo();

                Vuelo vuelo = new Vuelo();
                vuelo.Origen = ObtenerOrigen();
                vuelo.Destino = ObtenerDestino();
                vuelo.Escalas = _escalas;
                vuelo.Pasajeros = _pasajeros;
                vuelo.CantidadPasajerosLimite = int.Parse(cuCantidadPasajeros.ObtenerValor());

                _vueloBusiness.Crear(vuelo);

                MessageBox.Show("Vuelo creado con éxito!");
            }
            catch (ABMException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private Origen ObtenerOrigen()
        {
            Origen origen = new Origen((Ciudad)listCiudadOrigen.SelectedItem);

            origen.Despegue = dtDespegue.Value;

            return origen;
        }

        private Destino ObtenerDestino()
        {
            Destino destino = new Destino((Ciudad)listCiudadOrigen.SelectedItem);
            destino.Arribo = dtArribo.Value;
            destino.Internacional = chboxIternacional.Checked;

            return destino;

        }

        private void btnAgregarEscala_Click(object sender, EventArgs e)
        {
            if (_escalas == null)
                _escalas = new List<Escala>();

            Ciudad ciudad = (Ciudad)listEscala.SelectedItem;

            if (ciudad != null)
            {
                _escalas.Add(new Escala(ciudad));
                MessageBox.Show("Escala agregada satisfactoriamente.");

            }

        }

        private void btnAgregarPasajero_Click(object sender, EventArgs e)
        {
            if (_pasajeros == null)
                _pasajeros = new List<Pasajero>();

            Pasajero pasajero = (Pasajero)listPasajero.SelectedItem;

            if (pasajero != null)
            {
                _pasajeros.Add(pasajero);
                MessageBox.Show("Pasajero agregado satisfactoriamente.");
            }
        }

        private void ValidarVuelo()
        {
            if (_pasajeros == null || _pasajeros.Count == 0)
                throw new ABMException("Por favor, ingrese pasajeros al vuelo.");

            int limitePasajeros;
            
            if(!int.TryParse(cuCantidadPasajeros.ObtenerValor(), out limitePasajeros))
                throw new ABMException("Ingrese un valor correcto para la cantidad limite de pasajeros.");

            if (_pasajeros.Count > limitePasajeros)
                throw new ABMException("La cantidad de pasajeros no puede ser superior a la cantidad ingresada como límite.");
        }

        private void ValidarControles()
        {
            foreach (var control in gbVuelo.Controls)
            {
                if (control.GetType().Equals(typeof(CUTextoBox)))
                {
                    CUTextoBox ctr = (CUTextoBox)control;

                    if (!ctr.Validado)
                        throw new ABMException("Por favor, verifique los campos mal ingresados.");
                }
            }
        }
    }
}
