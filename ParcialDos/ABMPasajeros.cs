﻿using BE;
using BE.Helper;
using BLL;
using ParcialDos.CU;
using System;
using System.Windows.Forms;

namespace ParcialDos
{
    public partial class ABMPasajeros : Form
    {
        private readonly PasajeroBusiness _pasajeroBusiness = new PasajeroBusiness();
        public ABMPasajeros()
        {
            InitializeComponent();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                ValidarControles();

                Pasajero pasajero = MapearPasajero();

                _pasajeroBusiness.Crear(pasajero);
                CargarPasajeros();
            }
            catch (ABMException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ValidarControles()
        {
            foreach (var control in gbxAlta.Controls)
            {
                if (control.GetType().Equals(typeof(CUTextoBox)))
                {
                    CUTextoBox ctr = (CUTextoBox)control;

                    if (!ctr.Validado)
                        throw new ABMException("Por favor, verifique los campos mal ingresados.");
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            Pasajero pasajero = (Pasajero)lstbxPasajeros.SelectedItem;
            cuId.SetearValor(pasajero.Id.ToString());
            cuNombre.SetearValor(pasajero.Nombre);
            cuApellido.SetearValor(pasajero.Apellido);
            cuApellido.SetearValor(pasajero.DNI.ToString());
            cuId.SetearValor(pasajero.Id.ToString());

            CargarPasajeros();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            Pasajero pasajero = (Pasajero)lstbxPasajeros.SelectedItem;
            _pasajeroBusiness.Borrar(pasajero);
            CargarPasajeros();
        }

        private Pasajero MapearPasajero()
        {
            return new Pasajero
            {
                Id = string.IsNullOrEmpty(cuId.ObtenerValor()) ? 0 : int.Parse(cuId.ObtenerValor()),
                Nombre = (string)cuNombre.ObtenerValor(),
                Apellido = (string)cuApellido.ObtenerValor(),
                DNI = long.Parse(cuDNI.ObtenerValor())
            };
        }

        private void ABMPasajeros_Load(object sender, EventArgs e)
        {
            CargarPasajeros();
        }

        private void CargarPasajeros()
        {
            lstbxPasajeros.DataSource = null;
            lstbxPasajeros.DataSource = _pasajeroBusiness.Listar();
        }
    }
}
