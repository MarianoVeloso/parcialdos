﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcialDos
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void altaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ABMCiudad form = new ABMCiudad
            {
                MdiParent = this
            };

            form.Show();
        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMPasajeros form = new ABMPasajeros
            {
                MdiParent = this
            };

            form.Show();
        }

        private void altaDeVuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMVuelos form = new ABMVuelos
            {
                MdiParent = this
            };

            form.Show();
        }

        private void listadoDeVuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Informes form = new Informes
            {
                MdiParent = this
            };

            form.Show();
        }
    }
}
