﻿namespace ParcialDos
{
    partial class ABMVuelos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listCiudadOrigen = new System.Windows.Forms.ListBox();
            this.listPasajero = new System.Windows.Forms.ListBox();
            this.listCiudadDestino = new System.Windows.Forms.ListBox();
            this.listEscala = new System.Windows.Forms.ListBox();
            this.gbVuelo = new System.Windows.Forms.GroupBox();
            this.btnAgregarPasajero = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtEscalaPartida = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAgregarEscala = new System.Windows.Forms.Button();
            this.dtEscalaArribo = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.Pasajeros = new System.Windows.Forms.Label();
            this.cuCantidadPasajeros = new ParcialDos.CU.CUTextoBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDespegue = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtArribo = new System.Windows.Forms.DateTimePicker();
            this.chboxIternacional = new System.Windows.Forms.CheckBox();
            this.Destino = new System.Windows.Forms.Label();
            this.Origen = new System.Windows.Forms.Label();
            this.gbVuelo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listCiudadOrigen
            // 
            this.listCiudadOrigen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listCiudadOrigen.FormattingEnabled = true;
            this.listCiudadOrigen.Location = new System.Drawing.Point(12, 45);
            this.listCiudadOrigen.Name = "listCiudadOrigen";
            this.listCiudadOrigen.Size = new System.Drawing.Size(119, 173);
            this.listCiudadOrigen.TabIndex = 0;
            // 
            // listPasajero
            // 
            this.listPasajero.FormattingEnabled = true;
            this.listPasajero.Location = new System.Drawing.Point(262, 45);
            this.listPasajero.Name = "listPasajero";
            this.listPasajero.Size = new System.Drawing.Size(120, 173);
            this.listPasajero.TabIndex = 1;
            // 
            // listCiudadDestino
            // 
            this.listCiudadDestino.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listCiudadDestino.FormattingEnabled = true;
            this.listCiudadDestino.Location = new System.Drawing.Point(137, 45);
            this.listCiudadDestino.Name = "listCiudadDestino";
            this.listCiudadDestino.Size = new System.Drawing.Size(119, 173);
            this.listCiudadDestino.TabIndex = 2;
            // 
            // listEscala
            // 
            this.listEscala.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listEscala.FormattingEnabled = true;
            this.listEscala.Location = new System.Drawing.Point(6, 16);
            this.listEscala.Name = "listEscala";
            this.listEscala.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listEscala.Size = new System.Drawing.Size(119, 173);
            this.listEscala.TabIndex = 3;
            // 
            // gbVuelo
            // 
            this.gbVuelo.Controls.Add(this.btnAgregarPasajero);
            this.gbVuelo.Controls.Add(this.groupBox2);
            this.gbVuelo.Controls.Add(this.button1);
            this.gbVuelo.Controls.Add(this.Pasajeros);
            this.gbVuelo.Controls.Add(this.cuCantidadPasajeros);
            this.gbVuelo.Controls.Add(this.listPasajero);
            this.gbVuelo.Controls.Add(this.label2);
            this.gbVuelo.Controls.Add(this.dtDespegue);
            this.gbVuelo.Controls.Add(this.label1);
            this.gbVuelo.Controls.Add(this.dtArribo);
            this.gbVuelo.Controls.Add(this.chboxIternacional);
            this.gbVuelo.Controls.Add(this.Destino);
            this.gbVuelo.Controls.Add(this.Origen);
            this.gbVuelo.Controls.Add(this.listCiudadOrigen);
            this.gbVuelo.Controls.Add(this.listCiudadDestino);
            this.gbVuelo.Location = new System.Drawing.Point(13, 10);
            this.gbVuelo.Name = "gbVuelo";
            this.gbVuelo.Size = new System.Drawing.Size(778, 432);
            this.gbVuelo.TabIndex = 4;
            this.gbVuelo.TabStop = false;
            this.gbVuelo.Text = "Vuelo";
            // 
            // btnAgregarPasajero
            // 
            this.btnAgregarPasajero.Location = new System.Drawing.Point(262, 224);
            this.btnAgregarPasajero.Name = "btnAgregarPasajero";
            this.btnAgregarPasajero.Size = new System.Drawing.Size(120, 23);
            this.btnAgregarPasajero.TabIndex = 17;
            this.btnAgregarPasajero.Text = "Agregar pasajero";
            this.btnAgregarPasajero.UseVisualStyleBackColor = true;
            this.btnAgregarPasajero.Click += new System.EventHandler(this.btnAgregarPasajero_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.listEscala);
            this.groupBox2.Controls.Add(this.dtEscalaPartida);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnAgregarEscala);
            this.groupBox2.Controls.Add(this.dtEscalaArribo);
            this.groupBox2.Location = new System.Drawing.Point(426, 29);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(346, 199);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Escala";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Fecha Partida";
            // 
            // dtEscalaPartida
            // 
            this.dtEscalaPartida.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtEscalaPartida.Location = new System.Drawing.Point(134, 34);
            this.dtEscalaPartida.Name = "dtEscalaPartida";
            this.dtEscalaPartida.Size = new System.Drawing.Size(200, 20);
            this.dtEscalaPartida.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(131, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Fecha Arribo";
            // 
            // btnAgregarEscala
            // 
            this.btnAgregarEscala.Location = new System.Drawing.Point(193, 166);
            this.btnAgregarEscala.Name = "btnAgregarEscala";
            this.btnAgregarEscala.Size = new System.Drawing.Size(147, 23);
            this.btnAgregarEscala.TabIndex = 15;
            this.btnAgregarEscala.Text = "Agregar Escala";
            this.btnAgregarEscala.UseVisualStyleBackColor = true;
            this.btnAgregarEscala.Click += new System.EventHandler(this.btnAgregarEscala_Click);
            // 
            // dtEscalaArribo
            // 
            this.dtEscalaArribo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtEscalaArribo.Location = new System.Drawing.Point(134, 84);
            this.dtEscalaArribo.Name = "dtEscalaArribo";
            this.dtEscalaArribo.Size = new System.Drawing.Size(200, 20);
            this.dtEscalaArribo.TabIndex = 17;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(452, 403);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Crear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Pasajeros
            // 
            this.Pasajeros.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Pasajeros.AutoSize = true;
            this.Pasajeros.Location = new System.Drawing.Point(259, 29);
            this.Pasajeros.Name = "Pasajeros";
            this.Pasajeros.Size = new System.Drawing.Size(53, 13);
            this.Pasajeros.TabIndex = 13;
            this.Pasajeros.Text = "Pasajeros";
            // 
            // cuCantidadPasajeros
            // 
            this.cuCantidadPasajeros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cuCantidadPasajeros.Location = new System.Drawing.Point(12, 354);
            this.cuCantidadPasajeros.Name = "cuCantidadPasajeros";
            this.cuCantidadPasajeros.Size = new System.Drawing.Size(200, 52);
            this.cuCantidadPasajeros.TabIndex = 12;
            this.cuCantidadPasajeros.Texto = "Cantidad de pasajeros";
            this.cuCantidadPasajeros.Validacion = "^[0-9]+$";
            this.cuCantidadPasajeros.Validado = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fecha Partida";
            // 
            // dtDespegue
            // 
            this.dtDespegue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtDespegue.Location = new System.Drawing.Point(12, 278);
            this.dtDespegue.Name = "dtDespegue";
            this.dtDespegue.Size = new System.Drawing.Size(200, 20);
            this.dtDespegue.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 312);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Fecha Arribo";
            // 
            // dtArribo
            // 
            this.dtArribo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtArribo.Location = new System.Drawing.Point(12, 328);
            this.dtArribo.Name = "dtArribo";
            this.dtArribo.Size = new System.Drawing.Size(200, 20);
            this.dtArribo.TabIndex = 8;
            // 
            // chboxIternacional
            // 
            this.chboxIternacional.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chboxIternacional.AutoSize = true;
            this.chboxIternacional.Location = new System.Drawing.Point(12, 243);
            this.chboxIternacional.Name = "chboxIternacional";
            this.chboxIternacional.Size = new System.Drawing.Size(87, 17);
            this.chboxIternacional.TabIndex = 7;
            this.chboxIternacional.Text = "Internacional";
            this.chboxIternacional.UseVisualStyleBackColor = true;
            // 
            // Destino
            // 
            this.Destino.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Destino.AutoSize = true;
            this.Destino.Location = new System.Drawing.Point(134, 29);
            this.Destino.Name = "Destino";
            this.Destino.Size = new System.Drawing.Size(43, 13);
            this.Destino.TabIndex = 5;
            this.Destino.Text = "Destino";
            // 
            // Origen
            // 
            this.Origen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Origen.AutoSize = true;
            this.Origen.Location = new System.Drawing.Point(9, 29);
            this.Origen.Name = "Origen";
            this.Origen.Size = new System.Drawing.Size(38, 13);
            this.Origen.TabIndex = 4;
            this.Origen.Text = "Origen";
            // 
            // ABMVuelos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 456);
            this.Controls.Add(this.gbVuelo);
            this.Name = "ABMVuelos";
            this.Text = "ABMVuelos";
            this.Load += new System.EventHandler(this.ABMVuelos_Load);
            this.gbVuelo.ResumeLayout(false);
            this.gbVuelo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listCiudadOrigen;
        private System.Windows.Forms.ListBox listPasajero;
        private System.Windows.Forms.ListBox listCiudadDestino;
        private System.Windows.Forms.ListBox listEscala;
        private System.Windows.Forms.GroupBox gbVuelo;
        private System.Windows.Forms.Label Origen;
        private System.Windows.Forms.Label Destino;
        private System.Windows.Forms.CheckBox chboxIternacional;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtArribo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDespegue;
        private CU.CUTextoBox cuCantidadPasajeros;
        private System.Windows.Forms.Label Pasajeros;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAgregarEscala;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtEscalaPartida;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtEscalaArribo;
        private System.Windows.Forms.Button btnAgregarPasajero;
    }
}