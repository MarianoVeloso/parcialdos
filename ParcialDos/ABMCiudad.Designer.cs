﻿namespace ParcialDos
{
    partial class ABMCiudad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAlta = new System.Windows.Forms.GroupBox();
            this.linkLabel = new System.Windows.Forms.LinkLabel();
            this.btnCrear = new System.Windows.Forms.Button();
            this.lstbxCiudades = new System.Windows.Forms.ListBox();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.cuId = new ParcialDos.CU.CUTextoBox();
            this.cuLocalidad = new ParcialDos.CU.CUTextoBox();
            this.cuCoordenadasLongitud = new ParcialDos.CU.CUTextoBox();
            this.cuCoordenadasLatitud = new ParcialDos.CU.CUTextoBox();
            this.cuPais = new ParcialDos.CU.CUTextoBox();
            this.gbxAlta.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxAlta
            // 
            this.gbxAlta.Controls.Add(this.linkLabel);
            this.gbxAlta.Controls.Add(this.cuId);
            this.gbxAlta.Controls.Add(this.btnCrear);
            this.gbxAlta.Controls.Add(this.cuLocalidad);
            this.gbxAlta.Controls.Add(this.cuCoordenadasLongitud);
            this.gbxAlta.Controls.Add(this.cuCoordenadasLatitud);
            this.gbxAlta.Controls.Add(this.cuPais);
            this.gbxAlta.Location = new System.Drawing.Point(12, 12);
            this.gbxAlta.Name = "gbxAlta";
            this.gbxAlta.Size = new System.Drawing.Size(237, 240);
            this.gbxAlta.TabIndex = 4;
            this.gbxAlta.TabStop = false;
            this.gbxAlta.Text = "Alta";
            // 
            // linkLabel
            // 
            this.linkLabel.AutoSize = true;
            this.linkLabel.Location = new System.Drawing.Point(6, 211);
            this.linkLabel.Name = "linkLabel";
            this.linkLabel.Size = new System.Drawing.Size(139, 13);
            this.linkLabel.TabIndex = 9;
            this.linkLabel.TabStop = true;
            this.linkLabel.Text = "Coordenadas en navegador";
            this.linkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // btnCrear
            // 
            this.btnCrear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrear.Location = new System.Drawing.Point(156, 211);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(75, 23);
            this.btnCrear.TabIndex = 4;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = true;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // lstbxCiudades
            // 
            this.lstbxCiudades.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lstbxCiudades.FormattingEnabled = true;
            this.lstbxCiudades.Location = new System.Drawing.Point(289, 12);
            this.lstbxCiudades.Name = "lstbxCiudades";
            this.lstbxCiudades.Size = new System.Drawing.Size(332, 186);
            this.lstbxCiudades.TabIndex = 5;
            // 
            // btnBorrar
            // 
            this.btnBorrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBorrar.Location = new System.Drawing.Point(546, 229);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(75, 23);
            this.btnBorrar.TabIndex = 6;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar.Location = new System.Drawing.Point(465, 229);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 7;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // cuId
            // 
            this.cuId.Location = new System.Drawing.Point(190, 19);
            this.cuId.Name = "cuId";
            this.cuId.Size = new System.Drawing.Size(17, 15);
            this.cuId.TabIndex = 8;
            this.cuId.Texto = null;
            this.cuId.Validacion = null;
            this.cuId.Validado = true;
            this.cuId.Visible = false;
            // 
            // cuLocalidad
            // 
            this.cuLocalidad.Location = new System.Drawing.Point(15, 19);
            this.cuLocalidad.Name = "cuLocalidad";
            this.cuLocalidad.Size = new System.Drawing.Size(133, 39);
            this.cuLocalidad.TabIndex = 0;
            this.cuLocalidad.Texto = "Localidad";
            this.cuLocalidad.Validacion = "^[\\w ]+$";
            this.cuLocalidad.Validado = false;
            // 
            // cuCoordenadasLongitud
            // 
            this.cuCoordenadasLongitud.Location = new System.Drawing.Point(15, 154);
            this.cuCoordenadasLongitud.Name = "cuCoordenadasLongitud";
            this.cuCoordenadasLongitud.Size = new System.Drawing.Size(133, 39);
            this.cuCoordenadasLongitud.TabIndex = 3;
            this.cuCoordenadasLongitud.Texto = "Longitud";
            this.cuCoordenadasLongitud.Validacion = "^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6" +
    "})?))$";
            this.cuCoordenadasLongitud.Validado = false;
            // 
            // cuCoordenadasLatitud
            // 
            this.cuCoordenadasLatitud.Location = new System.Drawing.Point(14, 109);
            this.cuCoordenadasLatitud.Name = "cuCoordenadasLatitud";
            this.cuCoordenadasLatitud.Size = new System.Drawing.Size(133, 39);
            this.cuCoordenadasLatitud.TabIndex = 2;
            this.cuCoordenadasLatitud.Texto = "Latitud";
            this.cuCoordenadasLatitud.Validacion = "^(\\+|-)?(?:90(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,6})?))$";
            this.cuCoordenadasLatitud.Validado = false;
            // 
            // cuPais
            // 
            this.cuPais.Location = new System.Drawing.Point(15, 64);
            this.cuPais.Name = "cuPais";
            this.cuPais.Size = new System.Drawing.Size(133, 39);
            this.cuPais.TabIndex = 1;
            this.cuPais.Texto = "Pais";
            this.cuPais.Validacion = "^[\\w ]+$";
            this.cuPais.Validado = false;
            // 
            // ABMCiudad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 299);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.lstbxCiudades);
            this.Controls.Add(this.gbxAlta);
            this.Name = "ABMCiudad";
            this.Text = "ABMDestinos";
            this.Load += new System.EventHandler(this.ABMCiudad_Load);
            this.gbxAlta.ResumeLayout(false);
            this.gbxAlta.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CU.CUTextoBox cuLocalidad;
        private CU.CUTextoBox cuCoordenadasLatitud;
        private CU.CUTextoBox cuPais;
        private CU.CUTextoBox cuCoordenadasLongitud;
        private System.Windows.Forms.GroupBox gbxAlta;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.ListBox lstbxCiudades;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnEditar;
        private CU.CUTextoBox cuId;
        private System.Windows.Forms.LinkLabel linkLabel;
    }
}