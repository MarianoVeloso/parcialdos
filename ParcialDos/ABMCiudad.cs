﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BE.Helper;
using BLL;
using ParcialDos.CU;

namespace ParcialDos
{
    public partial class ABMCiudad : Form
    {
        private readonly CiudadBusiness _ciudadBusiness = new CiudadBusiness();
        public ABMCiudad()
        {
            InitializeComponent();
        }

        private void ABMCiudad_Load(object sender, EventArgs e)
        {
            CargarCiudades();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                ValidarControles();

                Ciudad ciudad = MapearCiudad();

                _ciudadBusiness.Crear(ciudad);
                CargarCiudades();
            }
            catch (ABMException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ValidarControles()
        {
            foreach (var control in gbxAlta.Controls)
            {
                if (control.GetType().Equals(typeof(CUTextoBox)))
                {
                    CUTextoBox ctr = (CUTextoBox)control;

                    if (!ctr.Validado)
                        throw new ABMException("Por favor, verifique los campos mal ingresados.");
                }
            }
        }

        private void CargarCiudades()
        {
            lstbxCiudades.DataSource = null;
            lstbxCiudades.DataSource = _ciudadBusiness.Listar();
        }


        private void btnEditar_Click(object sender, EventArgs e)
        {
            Ciudad ciudad = (Ciudad)lstbxCiudades.SelectedItem;
            cuPais.SetearValor(ciudad.Pais);
            cuCoordenadasLatitud.SetearValor(ciudad.Latitud.ToString());
            cuCoordenadasLongitud.SetearValor(ciudad.Longitud.ToString());
            cuLocalidad.SetearValor(ciudad.Localidad.ToString());
            cuId.SetearValor(ciudad.IdCiudad.ToString());
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            _ciudadBusiness.Borrar((Ciudad)lstbxCiudades.SelectedItem);

            CargarCiudades();
        }

        private Ciudad MapearCiudad()
        {
            return new Ciudad()
            {
                IdCiudad = string.IsNullOrEmpty(cuId.ObtenerValor()) ? 0 : int.Parse(cuId.ObtenerValor()),
                Pais = cuPais.ObtenerValor(),
                Latitud = cuCoordenadasLatitud.ObtenerValor(),
                Longitud = cuCoordenadasLongitud.ObtenerValor(),
                Localidad = cuLocalidad.ObtenerValor()
            };
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                ValidarControles();

                string direccion = "https://www.google.com/maps/@" + cuCoordenadasLatitud.ObtenerValor() 
                                                                    + "," + cuCoordenadasLongitud.ObtenerValor() + ",14z/";
                System.Diagnostics.Process.Start("chrome.exe", direccion);
                System.Diagnostics.Process.Start(direccion, " --new-window");
            }
            catch (ABMException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
